#!/bin/bash

# clone the repository in local
rm -rf /home/parag/pro/*
rm -rf /home/parag/pro/.*
git clone https://gitlab.com/paragvyas236/pr1.git -b master /home/parag/pro/

# 1. create a local Variables for directory location
project_directory="pro/"  #project directory
backup_directory="/home/parag/Back_up"       #directory where backups will be stored
google_drive_directory="test:Backup" #Google Drive folder to upload backups

#______________________________________________________________________________________________________

# 2. Rotational Backup Settings
DAILY_BACKUPS_RETENTION=7   #Number of daily backups to retain
WEEKLY_BACKUPS_RETENTION=4  # Number of weekly backups to retain
MONTHLY_BACKUPS_RETENTION=3 # Number of monthly backups to retain


#______________________________________________________________________________________________________

# 3. writing a Method for backup creation
create_backup() {
    local timestamp=$(date +"%Y-%m-%d_%H-%M-%S")
    local backup_name="backup_$timestamp.zip"
    local backup_path="$backup_directory/$timestamp"

    #Create backup directory
    mkdir -p "$backup_path"

    # Zip project folder
    zip -r "$backup_path/$backup_name" "$project_directory" || { echo "Failed to create backup"; exit 1; }

    # Push backup to Google Drive
    rclone -v sync "$backup_path/$backup_name" "$google_drive_directory" || { echo "Failed to upload backup to Google Drive"; exit 1; }

    #Delete older backups (calling delete_old_backups() method )
    delete_old_backups
}



#______________________________________________________________________________________________________


# 4. Writing a Method for delete old backup
delete_old_backups() {
  # 1. Delete daily backups older than retention period
    find "$backup_directory" -type d -mtime +$DAILY_BACKUPS_RETENTION -exec rm -rf {} \;

    # 2. Delete weekly backups older than retention period
    find "$backup_directory" -type d -mtime +$((WEEKLY_BACKUPS_RETENTION*7)) -exec rm -rf {} \;

    # 3. Delete monthly backups older than retention period
    find "$backup_directory" -type d -mtime +$((MONTHLY_BACKUPS_RETENTION*30)) -exec rm -rf {} \;
}

#______________________________________________________________________________________________________



# 5. writing a method to send cURL request on successful backup

send_curl_request() {
    local project_name="Backup_Store_in_Google_Drive"
    local backup_date=$(date +"%Y-%m-%d_%H-%M-%S")
    local test_identifier="Backup Successful"
    local url="https://webhook.site/6e918083-316e-4ae4-896a-d13f740d604d"

curl -X POST "$url" \
      -H 'content-type: application/json' \
      -d '{"project": "'"$project_name"'", "date": "'"$backup_date"'", "test": "'"$test_identifier"'"}'
}


#______________________________________________________________________________________________________


# 6. writing main method
main() {
    create_backup && send_curl_request 
}

#Execute main method
main



