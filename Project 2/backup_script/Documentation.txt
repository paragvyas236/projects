Backup Script Documentation
1. Overview :- 
The backup script automates the process of creating backups for a specified project directory, uploading them to Google Drive using rclone, and implementing a rotational backup strategy to manage backup retention. Additionally, it sends a cURL request upon successful backup completion.

2. Install rclone to our system and config the rclone with help of rclone offical website Documentation
        for installing rclone visit the official website of rclone :- https://rclone.org/install/   and copy the script of installation of rclone in linux    
                                    sudo -v ; curl https://rclone.org/install.sh | sudo bash

    after Installing rclone configure the rclone with google drive. visit the official website of rclone  :- https://rclone.org/drive/  and copy the config command and go through the interactive setup process  


3. Clone Repository :- 
Clone the repository locally using the provided GitLab URL :- https://gitlab.com/paragvyas236/pr1.git

4. Variables Configuration :-
Update the following variables in the script according to your setup:
project_directory :- Path to the project directory.
backup_directory :- Directory where backups will be stored locally.
google_drive_directory :- Google Drive folder where i we want to upload backups


5. Rotational Backup Settings :-
Adjust the values of DAILY_BACKUPS_RETENTION, WEEKLY_BACKUPS_RETENTION, and MONTHLY_BACKUPS_RETENTION variables to define the retention periods for daily, weekly, and monthly backups, respectively.


6. writing a method to create a backup and push in to the google drive
        6.1 writing a method to create a backup folder with time stamp
        6.2 writing a method to create a backup in .zip file
        6.3 after creating a .zip file we push the .zip into our google drive

7. Rotational Backup Strategy :- 
The script implements a rotational backup strategy to manage backup retention:
It deletes daily backups older than the specified retention period (DAILY_BACKUPS_RETENTION days).
It deletes weekly backups older than the specified retention period (WEEKLY_BACKUPS_RETENTION * 7 days).
It deletes monthly backups older than the specified retention period (MONTHLY_BACKUPS_RETENTION * 30 days).

8. cURL Request :- 
Upon successful backup completion, the script sends a cURL request with the backup details to a specified URL.
You can configure the cURL request details in the send_curl_request() method.



